<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pest".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $file_name
 */
class Pest extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'file_name' => 'File_Name',
        ];
    }
}
