<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pest2}}`.
 */
class m220913_130846_create_pest2_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pest2}}', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'preview' => $this->text(),
            'image' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220913_130846_create_pest2_table.\n";
        return false;
    }
}
