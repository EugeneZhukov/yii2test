<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users_bd}}`.
 */
class m220903_131556_create_users_bd_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users_bd}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'age' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->dropTable('{{%users_bd}}');
        echo "m220903_131556_create_users_bd_table.\n";

        return false;
    }
}
