<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pest}}`.
 */
class m220906_183946_create_pest_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pest}}', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'file_name' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       echo "m220906_183946_create_pest_table.\n";
       return false;
    }
}
