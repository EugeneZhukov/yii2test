<?php

use app\models\Pest;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model app\models\Pest */
/* @var $item app\models\Pest */

$path = '/web/uploads/pest-image/';
$pathSmall = '/web/uploads/pest-image-small/';
$model = Pest::find()->where([">", "id", 0])->all();


?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css"/>


    <div class="row">


        <?php foreach ($model

                       as $item): ?>

            <div class="col">
                <div class="card" style="height: 450px;">
                    <a data-fancybox="gallery" href="<?= $path . $item->file_name ?>" >
                        <img class="rounded" src="<?= $pathSmall . $item->file_name ?>" width="150px"/>
                    </a>
                    <div class="card-body">
                        <p><small><?= $item->name ?></small></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>


