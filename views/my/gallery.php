<?php

use app\models\Pest;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $model app\models\Pest */
/* @var $item app\models\Pest */

$path='/web/uploads/pest-image/';
$model = Pest::find()->where([">","id", 0])->all();


?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" />
<style>
    img {
        width: 500px;
    }
</style>

<script type="text/javascript" src = '/web/test/script test.js' ></script>
<div class="row">
<?php foreach($model as $item):?>
        <div class="col">
            <div class="card" style="height: 450px;">
                <img class="card-img-top" src="<?=$path.htmlspecialchars(urlencode($item->file_name))?>" data-fancybox="gallery" data-caption="<?=$item->name ?>" alt="<?=$item->file_name?>" style="width: 150px;">

                    <div class="card-body">
                        <p><small><?=$item->name ?></small></p>
                    </div>
            </div>
        </div>
<?php endforeach; ?>
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>






    <!---->
<!--    <img src="https://www.sunhome.ru/i/wallpapers/73/krasnoe-selo.orig.jpg" data-fancybox="gallery" data-caption="Описание 1" alt="">-->
<!--    <img src="https://img1.akspic.ru/attachments/crops/2/2/4/0/50422/50422-senokosnoye_ugodye-pole-selskoe_hozyajstvo-zakat-risovoe_pole-2560x1440.jpg" data-fancybox="gallery" data-caption="Описание 2" alt="">-->
<!--    <img src="https://mobimg.b-cdn.net/v3/fetch/89/89b1452e43e738be92c573fdebfb1d22.jpeg" data-fancybox="gallery" data-caption="Описание 3" alt="">-->
<!---->
<!--    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>-->
<!--    -->
