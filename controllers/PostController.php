<?php

namespace app\controllers;

use app\models\Pest;
use app\models\UsersBd;
use Psy\Readline\Hoa\FileException;
use Yii;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\web\Controller;


class PostController extends Controller
{
    public $enableCsrfValidation = false;



    /**
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionPostToBd()
    {

//      проверяем наличие директорий
        if (!file_exists('./uploads/pest-image/'))
            mkdir("./uploads/pest-image/", 0700, true);
        if (!file_exists('./uploads/pest-image-small/'))
            mkdir("./uploads/pest-image-small/", 0700, true);



        $this->enableCsrfValidation = false;

//          открываем транзакцию
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($this->request->isPost)
            {
                $status = 'метод верный---';
                foreach ($this->request->post() as $item)
                {
//                  создаем объект БД
                    $usersModel = new Pest();
//                  генерируем имя файла
                    $fileName = date('d_m_Y_his') . rand(1, 9999) . '.jpg';
//                  пишем в БД данные
                    $usersModel->name = $item['name'];
                    $usersModel->file_name = $fileName;
//                  сохраняем данные в БД
                    if (!$usersModel->save()){
                        throw new Exception('не удалось сохранить');
                    }


//                  берем картинку по ссылке
                    $client = new Client();
                    $response = $client->createRequest()
                        ->setMethod('GET')
                        ->setUrl($item['image'])
                        ->send();

                    if (file_put_contents("./uploads/pest-image/{$fileName}", $response->content)) {
                        $status = 'данные сохранены';
                    } else {
                        throw new Exception('ОШИБКА: файл не сохранён');
                    }

//                  берем картинку по ссылке 2
                    $client = new Client();
                    $response = $client->createRequest()
                        ->setMethod('GET')
                        ->setUrl($item['preview'])
                        ->send();

                    if (file_put_contents("./uploads/pest-image-small/{$fileName}", $response->content)) {
                        $status = 'данные сохранены';
                    } else {
                        throw new Exception('ОШИБКА: файл не сохранён');
                    }



                }

            } else {
                throw new Exception('неверный метод');
            }

//            коммитим транзакцию
            $transaction->commit();

        } catch (\Exception $e){
            $transaction->rollBack();
            throw $e;
        }


        return ($status);
    }


    public function actionGallery ()
    {
        return $this->render('gallery');





    }






}
